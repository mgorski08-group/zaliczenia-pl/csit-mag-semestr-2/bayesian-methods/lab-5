import arviz as az
import pymc3 as pm

with pm.Model() as model:
    p = pm.Uniform("p", lower=0, upper=1)
    obs = pm.Bernoulli("obs", p, observed=[1, 1, 1, 1, 1, 1, 1, 0, 0, 0])
    idata = pm.sample(2000, tune=2500)
az.plot_trace(idata, show=True)
